import 'package:flutter/material.dart';
import 'package:hiname/homepage.dart';

class LoginPage extends StatefulWidget {
  static String tag = 'login-page';

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController tname = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 48.0,
        child: Image.asset('assets/sttb.jpg'),
      ),
    );

    final titlename = Container(
      child: Center(
        child: Text(
          "Enter Your Name Here !",
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
    );

    final uname = TextField(
      controller: tname,
      keyboardType: TextInputType.name,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Your Name',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final loginbtn = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: Material(
        borderRadius: BorderRadius.circular(50.0),
        shadowColor: Colors.lightBlueAccent.shade50,
        elevation: 5.0,
        child: MaterialButton(
          minWidth: 200.0,
          height: 50.0,
          onPressed: () {
            var namasaya = tname.text;
            Navigator.push(context, MaterialPageRoute(
              builder: (_) {
                var homePage = HomePage(
                  namakamu: namasaya.toString(),
                );
                return homePage;
              },
            ));
          },
          color: Colors.lightBlueAccent,
          child: Text('Login', style: TextStyle(color: Colors.white)),
        ),
      ),
    );

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: Center(
        child: SingleChildScrollView(
          child: ListView(
            physics: NeverScrollableScrollPhysics(),
            // physics:BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              logo,
              SizedBox(height: 48.0),
              titlename,
              SizedBox(height: 18.0),
              uname,
              SizedBox(height: 8.0),
              loginbtn
            ],
          ),
        ),
      ),
      // )
    );
  }
}
