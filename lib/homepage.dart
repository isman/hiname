import 'package:flutter/material.dart';
import 'package:hiname/profilepage.dart';

class HomePage extends StatelessWidget {
  final namakamu;
  HomePage({@required this.namakamu});
  var namai = "Ade Isman Aji";
  var namaz = "Zeni Malik Abdullah";
  var namas = "Sindu Prakasa Lesmana";
  var namad = "Moch Dzulvie Aldyansyah";
  var npmi = "18 111 179";
  var npmz = "18 111 237";
  var npms = "18 111 297";
  var npmd = "18 111 211";
  var potoi = 'assets/isman.jpg';
  var potoz = 'assets/zeni.jpeg';
  var potos = 'assets/sindu.jpeg';
  var potod = 'assets/dzulvie.jpeg';
  var ttli = "Cilacap, 19 Januari 2001";
  var ttlz = "Bandung, 7 Juni 1997";
  var ttls = "Bandung, 2 Oktober 2000";
  var ttld = "Bandung, 22 Maret 1999";
  var pdit = "Profile Details";
  var sub = "Juragan";

  static String tag = 'home-page';

  @override
  Widget build(BuildContext context) {
    final titlenama = (() {
      if (namakamu == namai ||
          namakamu == namaz ||
          namakamu == namas ||
          namakamu == namad) {
        return Container(
          child: Padding(
            padding: EdgeInsets.only(top: 20, bottom: 20),
            child: Text(
              'Welcome $sub\n$namakamu',
              style: TextStyle(
                  fontSize: 28.0,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
          ),
        );
      } else {
        return Container(
          child: Padding(
            padding: EdgeInsets.only(top: 20, bottom: 20),
            child: Text(
              'Welcome $namakamu\nAnda bukan $sub',
              style: TextStyle(
                  fontSize: 28.0,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
          ),
        );
      }
    })();

    final body1 = Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10),
            topRight: Radius.circular(10),
            bottomLeft: Radius.circular(10),
            bottomRight: Radius.circular(10)),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        children: <Widget>[
          ListTile(
            leading: CircleAvatar(
              radius: 25.0,
              backgroundColor: Colors.transparent,
              backgroundImage: AssetImage(potoz),
            ),
            title: Text(namaz),
            subtitle: Text(npmz),
          ),
          RaisedButton(
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(
                builder: (_) {
                  var profilePage = ProfilePage(
                      namakamu: namaz, poto: potoz, ttl: ttlz, npm: npmz);
                  return profilePage;
                },
              ));
            },
            color: Colors.blueAccent,
            textColor: Colors.white,
            child: Text(pdit),
          ),
        ],
      ),
    );

    final body2 = Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10),
            topRight: Radius.circular(10),
            bottomLeft: Radius.circular(10),
            bottomRight: Radius.circular(10)),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        children: <Widget>[
          ListTile(
            leading: CircleAvatar(
              radius: 25.0,
              backgroundColor: Colors.transparent,
              backgroundImage: AssetImage(potos),
            ),
            title: Text(namas),
            subtitle: Text(npms),
          ),
          RaisedButton(
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(
                builder: (_) {
                  var profilePage = ProfilePage(
                      namakamu: namas, poto: potos, ttl: ttls, npm: npms);
                  return profilePage;
                },
              ));
            },
            color: Colors.blueAccent,
            textColor: Colors.white,
            child: Text(pdit),
          ),
        ],
      ),
    );

    final body3 = Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10),
            topRight: Radius.circular(10),
            bottomLeft: Radius.circular(10),
            bottomRight: Radius.circular(10)),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        children: <Widget>[
          ListTile(
            leading: CircleAvatar(
              radius: 25.0,
              backgroundColor: Colors.transparent,
              backgroundImage: AssetImage(potod),
            ),
            title: Text(namad),
            subtitle: Text(npmd),
          ),
          RaisedButton(
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(
                builder: (_) {
                  var profilePage = ProfilePage(
                    namakamu: namad,
                    poto: potod,
                    ttl: ttld,
                    npm: npmd,
                  );
                  return profilePage;
                },
              ));
            },
            color: Colors.blueAccent,
            textColor: Colors.white,
            child: Text(pdit),
          ),
        ],
      ),
    );

    final body4 = Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10),
            topRight: Radius.circular(10),
            bottomLeft: Radius.circular(10),
            bottomRight: Radius.circular(10)),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        children: <Widget>[
          ListTile(
            leading: CircleAvatar(
              radius: 25.0,
              backgroundColor: Colors.transparent,
              backgroundImage: AssetImage(potoi),
            ),
            title: Text(namai),
            subtitle: Text(npmi),
          ),
          RaisedButton(
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(
                builder: (_) {
                  var profilePage = ProfilePage(
                    namakamu: namai,
                    poto: potoi,
                    ttl: ttli,
                    npm: npmi,
                  );
                  return profilePage;
                },
              ));
            },
            color: Colors.blueAccent,
            textColor: Colors.white,
            child: Text(pdit),
          ),
        ],
      ),
    );

    final body = Container(
      width: MediaQuery.of(context).size.width,
      // height: MediaQuery.of(context).size.height,
      // padding: EdgeInsets.all(28.0),
      child: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        padding: EdgeInsets.all(28.0),
        child: Column(
          children: <Widget>[
            titlenama,
            body1,
            SizedBox(height: 20),
            body2,
            SizedBox(height: 20),
            body3,
            SizedBox(height: 20),
            body4
          ],
        ),
      ),
    );

    return Scaffold(
      body: body,
    );
  }
}
