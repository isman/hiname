import 'package:flutter/material.dart';
import 'package:hiname/splashscreen.dart';
import 'package:hiname/loginpage.dart';
import 'package:hiname/homepage.dart';
import 'package:hiname/profilepage.dart';

void main() { 
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final routes = <String, WidgetBuilder>{
    LoginPage.tag: (context) => LoginPage(),
    HomePage.tag: (context) => HomePage(),
    ProfilePage.tag: (context) => ProfilePage(),
  }
  ;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Hi With Yourname',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.lightBlue,
        fontFamily: 'Nunito',
      ),
      home: Splash(),
      routes: routes,
    );
  }
}

// https://www.codepolitan.com/mudahnya-membuat-antarmuka-login-cantik-dengan-flutter-5aa46d29e03d1
// https://flutterawesome.com/clean-and-simple-login-ui-screen-with-a-basic-hero-animation-in-flutter/
// https://morioh.com/p/98894cc3a48d
