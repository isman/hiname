import 'package:flutter/material.dart';

class ProfilePage extends StatelessWidget {
  final namakamu, poto, ttl, npm;
  ProfilePage(
      {@required this.namakamu,
      @required this.poto,
      @required this.ttl,
      @required this.npm});

  static String tag = 'profile-page';

  @override
  Widget build(BuildContext context) {
    final photo = Hero(
      tag: 'hero',
      child: Padding(
        padding: EdgeInsets.only(top: 20.0),
        child: CircleAvatar(
          radius: 72.0,
          backgroundColor: Colors.transparent,
          backgroundImage: AssetImage('$poto'),
        ),
      ),
    );

    final titlenpm = Padding(
      padding: EdgeInsets.only(top: 10, bottom: 0),
      child: Text(
        'NPM Kamu',
        style: TextStyle(fontSize: 28.0, color: Colors.white),
      ),
    );

    final npmel = Padding(
      // padding: const EdgeInsets.fromLTRB(20, 25, 20, 4),
      padding: const EdgeInsets.fromLTRB(0, 25, 25, 25),
      child: Container(
        height: 60,
        child: Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              '$npm',
              style: TextStyle(color: Colors.white70),
            ),
          ),
        ),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(20)),
            border: Border.all(width: 1.0, color: Colors.white70)),
      ),
    );

    final titlenama = Padding(
      padding: EdgeInsets.only(top: 10, bottom: 0),
      child: Text(
        'Nama Kamu',
        style: TextStyle(fontSize: 28.0, color: Colors.white),
      ),
    );

    final nama = Padding(
      // padding: const EdgeInsets.fromLTRB(20, 25, 20, 4),
      padding: const EdgeInsets.fromLTRB(0, 25, 25, 25),
      child: Container(
        height: 60,
        child: Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              '$namakamu',
              style: TextStyle(color: Colors.white70),
            ),
          ),
        ),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(20)),
            border: Border.all(width: 1.0, color: Colors.white70)),
      ),
    );

    final titlettl = Padding(
      padding: EdgeInsets.only(top: 10, bottom: 0),
      child: Text(
        'Tempat & Tanggal Kamu Lahir',
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 28.0, color: Colors.white),
      ),
    );

    final ttlel = Padding(
      padding: const EdgeInsets.fromLTRB(5, 25, 25, 25),
      child: Container(
        height: 60,
        child: Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              '$ttl',
              style: TextStyle(color: Colors.white70),
            ),
          ),
        ),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(20)),
            border: Border.all(width: 1.0, color: Colors.white70)),
      ),
    );

    final titlealamat = Padding(
      padding: EdgeInsets.only(top: 10, bottom: 0),
      child: Text(
        'Alamat Kamu',
        style: TextStyle(fontSize: 28.0, color: Colors.white),
      ),
    );

    final alamat = Padding(
      padding: const EdgeInsets.fromLTRB(5, 25, 25, 25),
      child: Container(
        height: 60,
        child: Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              'Bandung',
              style: TextStyle(color: Colors.white70),
            ),
          ),
        ),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(20)),
            border: Border.all(width: 1.0, color: Colors.white70)),
      ),
    );

    final titlehoby = Padding(
      padding: EdgeInsets.only(top: 10, bottom: 0),
      child: Text(
        'Hobi Kamu',
        style: TextStyle(fontSize: 28.0, color: Colors.white),
      ),
    );

    final hoby = Padding(
      padding: const EdgeInsets.fromLTRB(5, 25, 25, 25),
      child: Container(
        height: 60,
        child: Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              'Cari uang',
              style: TextStyle(color: Colors.white70),
            ),
          ),
        ),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(20)),
            border: Border.all(width: 1.0, color: Colors.white70)),
      ),
    );

    final body = Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        gradient: LinearGradient(colors: [
          Colors.blue,
          Colors.lightBlueAccent,
        ]),
      ),
      child: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        padding: EdgeInsets.all(28.0),
        child: Column(
          children: <Widget>[
            photo,
            titlenpm,
            npmel,
            titlenama,
            nama,
            titlettl,
            ttlel,
            titlealamat,
            alamat,
            titlehoby,
            hoby
          ],
        ),
      ),
    );

    return Scaffold(
      body: body,
    );
  }
}
