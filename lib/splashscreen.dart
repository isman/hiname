import 'package:flutter/material.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:hiname/loginpage.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  @override
  Widget build(BuildContext context) {
    return new SplashScreen(
        seconds: 7,
        navigateAfterSeconds: new LoginPage(),
        title: new Text(
          'Tugas Mobile Programming\nAde Isman Aji\t18 111 179\nMoch Dzulvie Aldyansyah\t18 111 211\nSindu Prakasa Lesmana\t18 111 297\nZeni Malik Abdullah\t18 111 237\nTIF RM 18 CNS\nSTTB',
          style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
        ),
        backgroundColor: Colors.white,
        styleTextUnderTheLoader: new TextStyle(),
        photoSize: 100.0,
        onClick: () => print("Tugas"),
        loaderColor: Colors.red);
  }
}
