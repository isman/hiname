# hiname

Tugas Projek Flutter Mobile Programming.

Anggota :
1. Ade Isman Aji            - 18 111 179
2. Moch Dzulvie Aldyansyah  - 18 111 211
3. Sindu Prakasa Lesmana    - 18 111 297
4. Zeni Malik Abdulah       - 18 111 237

[Demo App](https://gitlab.com/isman17/hiname/-/blob/master/hiname.apk)

[Demo Screen](https://gitlab.com/isman17/hiname/-/blob/master/demo-hiname.mp4)

